/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.data.accessobjects;

import com.test.data.Employee;
import com.test.data.SearchCriteria;
import com.test.database.DatabaseWork;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author daemon
 */
public class EmployeeDAO implements GenericDAO<Employee>{
    private Connection connection;    
    private PreparedStatement preparedStatement = null;
    private ResultSet rs = null;
     
    public EmployeeDAO() {
        connect();
    }
    
    private void connect() {
        try {
            connection = DatabaseWork.getConnection().getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public long create(Employee employee) {
        long id = 0;
        try {
            connect();
            preparedStatement = connection.prepareStatement("INSERT INTO employees (firstname, lastname, fathername, phone, department, salary) VALUES (?, ?, ?, ?, ?, ?)");
            preparedStatement.setString(1, employee.getFirstname());
            preparedStatement.setString(2, employee.getLastname());
            preparedStatement.setString(3, employee.getFathername());
            preparedStatement.setString(4, employee.getPhone());
            preparedStatement.setLong(5, employee.getDepartment());
            preparedStatement.setInt(6, employee.getSalary());
            preparedStatement.executeUpdate();
            Statement statement = connection.createStatement();
            rs = statement.executeQuery("SELECT LAST_INSERT_ID()");
            while (rs.next()) {
                id = rs.getLong(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                    preparedStatement = null;
                } catch (SQLException e) { /* ignored */}
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
        return id;
    }

    @Override
    public Employee read(long id) {
        Employee employee = new Employee();
        try {
            connect();
            preparedStatement = connection.prepareStatement("SELECT * FROM employees WHERE id = ?");
            preparedStatement.setLong(1, id);
            rs = preparedStatement.executeQuery();

            if (rs.next()) {
                employee.setId(rs.getLong("id"));
                employee.setFirstname(rs.getString("firstname"));
                employee.setLastname(rs.getString("lastname"));
                employee.setFathername(rs.getString("fathername"));
                employee.setPhone(rs.getString("phone"));
                employee.setDepartment(rs.getLong("department"));
                employee.setSalary(rs.getInt("salary"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                    preparedStatement = null;
                } catch (SQLException e) { /* ignored */}
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
        
        return employee;
    }

    @Override
    public List<Employee> readAll(String where, String limit, String order) {
        List<Employee> employees = new ArrayList<Employee>();
        try {
            connect();
            Statement statement = connection.createStatement();
            rs = statement.executeQuery("SELECT * FROM employees " + where + " " + limit + " " + order);
            while (rs.next()) {
                Employee employee = new Employee();
                employee.setId(rs.getLong("id"));
                employee.setFirstname(rs.getString("firstname"));
                employee.setLastname(rs.getString("lastname"));
                employee.setFathername(rs.getString("fathername"));
                employee.setPhone(rs.getString("phone"));
                employee.setDepartment(rs.getLong("department"));
                employee.setSalary(rs.getInt("salary"));
                employees.add(employee);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                    rs = null;
                } catch (SQLException e) { /* ignored */}
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                    preparedStatement = null;
                } catch (SQLException e) { /* ignored */}
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) { /* ignored */}
            }
        }

        return employees;
    }

    @Override
    public void update(Employee employee) {
        try {
            connect();
            preparedStatement = connection.prepareStatement("UPDATE employees SET firstname = ?, lastname = ?, fathername = ?, phone = ?, department = ?, salary = ? WHERE id = ?");
            preparedStatement.setString(1, employee.getFirstname());
            preparedStatement.setString(2, employee.getLastname());
            preparedStatement.setString(3, employee.getFathername());
            preparedStatement.setString(4, employee.getPhone());
            preparedStatement.setLong(5, employee.getDepartment());
            preparedStatement.setInt(6, employee.getSalary());
            preparedStatement.setLong(7, employee.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                    rs = null;
                } catch (SQLException e) { /* ignored */}
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                    preparedStatement = null;
                } catch (SQLException e) { /* ignored */}
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
    }

    @Override
    public void delete(long id) {
        try {
            connect();
            preparedStatement = connection.prepareStatement("DELETE FROM employees WHERE id = ?");
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                    rs = null;
                } catch (SQLException e) { /* ignored */}
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                    preparedStatement = null;
                } catch (SQLException e) { /* ignored */}
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
    }
    
    public int getLastId() {
        int lastid = 0;
        try {
            Statement statement = connection.createStatement();
            rs = statement.executeQuery("SELECT LAST_INSERT_ID()");
            lastid = -1;
            while(rs.next()) {
               lastid = Integer.parseInt(rs.getString(1));  
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) { /* ignored */}
            }
        }      
    return lastid;  
    }
    
    public int entriesQuantity(String where) {
        int quantity = 0;
        try {
            connect();
            Statement statement = connection.createStatement();
            rs = statement.executeQuery("SELECT COUNT(*) FROM employees " + where);
            while(rs.next()) {                
                quantity = Integer.parseInt(rs.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                    rs = null;
                } catch (SQLException e) { /* ignored */}
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
        return quantity;
    }
}
