/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.data;

/**
 *
 * @author daemon
 */
public class Department {
    
    private long id;
    private String name;
    private long head_id;
    
    public Department() {
    }
    
    public Department(String name, long head_id) {
        this.name = name;
        this.head_id = head_id;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the head_id
     */
    public long getHead_id() {
        return head_id;
    }

    /**
     * @param head_id the head_id to set
     */
    public void setHead_id(long head_id) {
        this.head_id = head_id;
    }
    
    
}
