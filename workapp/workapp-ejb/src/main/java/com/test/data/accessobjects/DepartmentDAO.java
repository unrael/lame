/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.data.accessobjects;

import com.test.data.Department;
import com.test.data.SearchCriteria;
import com.test.database.DatabaseWork;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author daemon
 */
public class DepartmentDAO implements GenericDAO<Department>{

    private Connection connection;
    private PreparedStatement preparedStatement = null;
    private ResultSet rs = null;
     
    public DepartmentDAO() {
        connect();
    }
    
    private void connect() {
        try {
            connection = DatabaseWork.getConnection().getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public long create(Department department) {
        long id = 0;
        try {            
            connect();
            preparedStatement = connection.prepareStatement("INSERT INTO departments (name, head) VALUES (?, ?)");
            preparedStatement.setString(1, department.getName());
            preparedStatement.setLong(2, department.getHead_id());
            preparedStatement.executeUpdate();
            Statement statement = connection.createStatement();
            rs = statement.executeQuery("SELECT LAST_INSERT_ID()");
            while (rs.next()) {
                id = rs.getLong(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
        return id;
    }

    @Override
    public Department read(long id) {
        Department department = new Department();
        try {            
            connect();
            preparedStatement = connection.prepareStatement("SELECT * FROM departments WHERE id = ?");
            preparedStatement.setLong(1, id);
            rs = preparedStatement.executeQuery();
            
            if (!rs.isBeforeFirst()) {
                return null;
            }

            if (rs.next()) {
                department.setId(rs.getLong("id"));
                department.setName(rs.getString("name"));
                department.setHead_id(rs.getInt("head"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
        return department;
    }

    @Override
    public List<Department> readAll(String where, String limit, String order) {
        List<Department> departments = new ArrayList<Department>();
        try {            
            connect();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM departments " + where + " " + limit + " " + order);
            while (rs.next()) {
                Department department = new Department();
                department.setId(rs.getLong("id"));
                department.setName(rs.getString("name"));
                department.setHead_id(rs.getInt("head"));
                departments.add(department);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return departments;
    }

    @Override
    public void update(Department department) {
        try {            
            connect();
            preparedStatement = connection.prepareStatement("UPDATE departments SET name = ?, head = ? WHERE id = ?");
            preparedStatement.setString(1, department.getName());
            preparedStatement.setLong(2, department.getHead_id());
            preparedStatement.setLong(3, department.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
    }

    @Override
    public void delete(long id) {
        try {            
            connect();
            preparedStatement = connection.prepareStatement("DELETE FROM departments WHERE id = ?");
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
    }
    
    public int getLastId() {
        int lastid = 0;
        try {
            Statement statement = connection.createStatement();
            rs = statement.executeQuery("SELECT LAST_INSERT_ID()");
            lastid = -1;
            while(rs.next()) {
               lastid = Integer.parseInt(rs.getString(1));  
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) { /* ignored */}
            }
        }      
    return lastid;  
    }
    
    public int entriesQuantity(String where) {
        int quantity = 0;
        try {
            Statement statement = connection.createStatement();
            rs = statement.executeQuery("SELECT COUNT(*) FROM departments" + where);                
            while(rs.next()) {                
                quantity = Integer.parseInt(rs.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
        return quantity;
    }
}