/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.data;

/**
 *
 * @author daemon
 */
public class Employee {
    
    private long id;
    private String firstname;
    private String lastname;
    private String fathername;
    private String phone;
    private long department;
    private int salary;
    
    public Employee() {
    }
    
    public Employee(String firstname,
                  String lastname,
                  String fathername,
                  String phone,
                  int department,
                  int salary) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.fathername = fathername;
        this.phone = phone;
        this.department = department;
        this.salary = salary;
    }
    
    /**
     * @return the id
     */
    public long getId() {
        return id;
    }
    
    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @return the fathername
     */
    public String getFathername() {
        return fathername;
    }

    /**
     * @param fathername the fathername to set
     */
    public void setFathername(String fathername) {
        this.fathername = fathername;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the department
     */
    public long getDepartment() {
        return department;
    }

    /**
     * @param department the department to set
     */
    public void setDepartment(long department) {
        this.department = department;
    }

    /**
     * @return the salary
     */
    public int getSalary() {
        return salary;
    }

    /**
     * @param salary the salary to set
     */
    public void setSalary(int salary) {
        this.salary = salary;
    }
    
}
