/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.data;

/**
 *
 * @author daemon
 */
public enum SearchCriteria {
    EMPLOYEEID,
    DEPARTMENTID,
    FULLNAME,
    FIRSTNAME,
    LASTNAME,
    FATHERNAME,
    PHONE,
    DEPARTNAME,
    DEPARTHEAD,
    SALARY,
    DEPARTEMPLOYEES
}
