/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.data.accessobjects;

import java.util.List;

/**
 *
 * @author daemon
 */

public interface GenericDAO<Entity extends Object> {
    public long create(Entity entity);
    public Entity read(long id);
    public List<Entity> readAll(String where, String limit, String order);
    public void update(Entity entity);
    public void delete(long id);
}