/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.database;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.sql.DataSource;


/**
 *
 * @author daemon
 */
@Stateless
@LocalBean
public class DatabaseWork {
    private static InitialContext ctx;
    private static DataSource ds;

    public static DataSource getConnection(){
        if (ds != null)
            return ds;
        else {
            try {
                ctx = new InitialContext();
                ds = (DataSource) ctx.lookup("jdbc/MySQLAppSource");
                return ds;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return ds;
        }
    }
}