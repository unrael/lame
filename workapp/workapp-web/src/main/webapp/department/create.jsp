<%-- 
    Document   : create
    Created on : 17.08.2015, 23:12:05
    Author     : daemon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui.css" type="text/css" media="all">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui.structure.css" type="text/css" media="all">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui.theme.css" type="text/css" media="all">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Create new department</title>
        <script src="${pageContext.request.contextPath}/js/jquery-2.1.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/jquery-ui.js"></script>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css" media="all">
    </head>
    <body>
        <h2>Create new department</h2>
        <form method="post" action="${pageContext.request.contextPath}/department/create">
            <table>
                <tr>
                    <td>
                        Name:
                    </td>
                    <td>
                        <input type="text" name="name" placeholder="Prinston Plainsboro" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        Head:
                    </td>
                    <td>
                        <input type="text" name="visiblehead" id="autocomplete">
                        <input type="hidden" name="head" id="invisiblehead">
                    </td>
                </tr>
            </table>
            
            <input type="submit" value="Create">            
        </form>
        <button onclick="javascript:history.back()">Cancel</button>
    </body>
    <script>
        $("#autocomplete").autocomplete({            
            source: function(request, response) {
                $.post("${pageContext.request.contextPath}/employee/search", {fullname: $("#autocomplete").val()}, function(responseJson) {
                    response(responseJson);
                });
            },
            focus: function( event, ui ) {
                $("#autocomplete" ).val(ui.item.firstname + " " + ui.item.lastname);
                return false;
            },
            select: function( event, ui ) {
                $("#autocomplete" ).val(ui.item.firstname + " " + ui.item.lastname);
                $("#invisiblehead").val(ui.item.id);
                return false;
            }
        })
        .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li>" ).append( "# " + item.id + " " + item.firstname + " " + item.lastname ).appendTo( ul );
        };
    </script>
    
</html>
