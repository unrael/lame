<%-- 
    Document   : update
    Created on : 18.08.2015, 6:12:42
    Author     : daemon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>        
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui.css" type="text/css" media="all">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui.structure.css" type="text/css" media="all">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui.theme.css" type="text/css" media="all">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit department #${department.id}</title>
        <script src="${pageContext.request.contextPath}/js/jquery-2.1.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/jquery-ui.js"></script>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css" media="all">
    </head>
    <body>
        <h2>Edit department #${department.id} ${department.name}</h2>
        <form method="post" action="${pageContext.request.contextPath}/department/update">
            <table>
                <tr>
                    <td>
                        ID:
                    </td>
                    <td>
                        <input type="text" name="visibleid" value=${department.id} required disabled>
                        <input type="hidden" name="id" value=${department.id}>
                    </td>
                </tr>
                <tr>
                    <td>
                        Name:
                    </td>
                    <td>
                        <input type="text" name="name" value="${department.name}" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        Head:
                    </td>
                    <td>
                        <input type="text" name="visiblehead" id="autocomplete" value="${head.firstname} ${head.lastname}">
                        <input type="hidden" name="head" id="invisiblehead" value=${department.head_id}>
                    </td>
                </tr>
            </table>
            <input type="hidden" name="updated" value="1">
            <input type="submit" value="Save">
        </form>
        <button onclick="javascript:history.back()">Cancel</button>
    </body>
    <script>
        $("#autocomplete").autocomplete({            
            source: function(request, response) {
                $.post("${pageContext.request.contextPath}/employee/search", {fullname: $("#autocomplete").val()}, function(responseJson) {
                    response(responseJson);
                });
            },
            focus: function( event, ui ) {
                $("#autocomplete" ).val(ui.item.firstname + " " + ui.item.lastname);
                return false;
            },
            select: function( event, ui ) {
                $("#autocomplete" ).val(ui.item.firstname + " " + ui.item.lastname);
                $("#invisiblehead").val(ui.item.id);
                return false;
            }
        })
        .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li>" ).append( "# " + item.id + " " + item.firstname + " " + item.lastname ).appendTo( ul );
        };
    </script>
</html>
