<%-- 
    Document   : workers
    Created on : 15.08.2015, 5:34:55
    Author     : daemon
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="${pageContext.request.contextPath}/js/jquery-2.1.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/jquery-ui.js"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.noty.packaged.min.js"></script>
        <title>All departments</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui.css">        
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui.structure.css">        
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui.theme.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css" media="all">
    </head>
    <body>
        <div id="container" class="ui-notify"></div>
        
        <form action="${pageContext.request.contextPath}/department/create" method="post" >
            <p><input type="submit" value="Add new department"></p>
        </form>   
            <a href="${pageContext.request.contextPath}/employee">All employees</a>
            <a href="${pageContext.request.contextPath}/department">All departments</a>
        <hr>
        <table border="1">
            <tr>
                <td>ID</td>
                <td>Name</td>
                <td>Head</td>
                <td></td>
                <td></td>
            </tr>
            <c:forEach items="${departments}" var="department">
                <tr>
                    <td style="text-align: center">
                        <a href="${pageContext.request.contextPath}/department?id=${department.id}">${department.id}</a>
                    </td>
                    <td>${department.name}</td>
                    <td>
                        <c:forEach items="${heads}" var="head">                            
                            <c:if test="${department.id == head.department}">
                                <a href="${pageContext.request.contextPath}/employee?id=${head.id}">${head.firstname} ${head.lastname}</a>
                            </c:if>
                        </c:forEach>
                    </td>
                    <td>
                        <form action="${pageContext.request.contextPath}/department/update" method="post">
                            <input type="hidden" name="id" value="${department.id}">
                            <input type = "submit" value="Edit">
                        </form>
                    </td>
                    <td>
                        <button value="${department.id}" class="deleteBtn">Delete</button>
                    </td>
                </tr>
            </c:forEach>
        </table>
        
        <form  id = "deletionForm" action="${pageContext.request.contextPath}/department/delete" method="post">
            <input id="deletionFormInput" type="hidden" name="deleteId" value="">
        </form>
        
        <div id="dialog-confirm" title="Delete department?">
            <p id="dialog-message"></p>
        </div>
        
    </body>
    <script>
        $(document).ready(function() {
            <c:if test="${deleted.id > 0}">
                var n = noty({
                    text: 'Department #${deleted.id} ${deleted.name} was successfully deleted',
                    layout: 'bottomRight',
                    type: 'success'
                });
            </c:if>
        });
        
        $(".deleteBtn").click(function() {
            var recordId = this.value;
            $("#dialog-message").html('<p id="dialog-message"><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This item will be permanently deleted and cannot be recovered. All employees from this department will be moved to Department #1. Are you sure?</p>');
                    
            $("#dialog-confirm").dialog({
                resizable: false,
                height: 180,
                width: 350,
                modal: true,
                buttons: {
                    "Delete this department": function() {
                        $('#deletionFormInput').val(recordId);
                        $('#deletionForm').submit();
                        
                    },
                    Cancel: function() {
                        $( this ).dialog( "close" );
                    }
                }
            });
        });
    </script>
</html>
