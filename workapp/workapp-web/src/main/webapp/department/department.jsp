<%-- 
    Document   : worker
    Created on : 15.08.2015, 5:34:48
    Author     : daemon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="/js/jquery-2.1.4.min.js"></script>
        <title>#${department.id} ${department.name}</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css" media="all">
    </head>
    <body>
        <div id="employee-name">
            #${department.id} ${department.name}
        </div>
        <table border="1">
            <tr>
                <td>ID</td>
                <td>${department.id}</td>
            </tr>
            <tr>
                <td>Name</td>
                <td>${department.name}</td>
            </tr>
            <tr>
                <td>Head</td>
                <td><a href = "${pageContext.request.contextPath}/employee?id=${head.id}"> ${head.firstname} ${head.lastname}</a></td>
            </tr>
        </table>
            
            <form action="/department/update" method="post">
                <input type="hidden" name="id" value="${department.id}">
                <input type = "submit" value="Edit">
            </form>

            <form action="/department/delete" method="post">
                <input type="hidden" name="id" value="${department.id}">
                <input type = "submit" value="Delete">
            </form>
        
            <a href="${pageContext.request.contextPath}/department">Go Back to All Departments</a>
        
            <h3>Employees of #${department.id} ${department.name}</h3>        
            
            <table border="1">
            <tr>
                <td>ID</td>
                <td>First name</td>
                <td>Last name</td>
                <td>Father name</td>
                <td>Phone</td>
                <td>Salary</td>
            </tr>
            <c:forEach items="${employees}" var="employee">
                <tr>
                    <td style="text-align: center">
                        <a href="${pageContext.request.contextPath}/employee?id=${employee.id}">${employee.id}</a>
                    </td>
                    <td>${employee.firstname}</td>
                    <td>${employee.lastname}</td>
                    <td>${employee.fathername}</td>
                    <td>${employee.phone}</td>
                    <td>${employee.salary}</td>
                </tr>
            </c:forEach>
        </table> 
        
        <c:if test="${pages > 1}">
            <c:if test="${currentPage != 1}">
                <td><a href="${pageContext.request.contextPath}/department?id=${department.id}&page=${currentPage - 1}">Previous</a></td>
            </c:if>

            <table border="1" cellpadding="5" cellspacing="5">
                <tr>
                    <c:forEach begin="1" end="${pages}" var="i">
                        <c:choose>
                            <c:when test="${currentPage eq i}">
                                <td>${i}</td>
                            </c:when>
                            <c:otherwise>
                                <td><a href="${pageContext.request.contextPath}/department?id=${department.id}&page=${i}">${i}</a></td>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </tr>
            </table>

            <c:if test="${currentPage lt pages}">
                <td><a href="${pageContext.request.contextPath}/department?id=${department.id}&page=${currentPage + 1}">Next</a></td>
            </c:if>
        </c:if>    
    </body>
</html>
