<%-- 
    Document   : index
    Created on : 18.08.2015, 6:12:23
    Author     : daemon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Error</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css" media="all">
    </head>
    
    <body>
        <b>Error:</b> <i>${message}</i>
        <br />
        <a href="${pageContext.request.contextPath}/">Go to the home page</a>
    </body>
</html>
