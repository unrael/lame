<%-- 
    Document   : index
    Created on : 15.08.2015, 3:36:18
    Author     : daemon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>        
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui.css" type="text/css" media="all">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui.structure.css" type="text/css" media="all">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui.theme.css" type="text/css" media="all">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Main page</title>
        <script src="${pageContext.request.contextPath}/js/jquery-2.1.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/jquery-ui.js"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.noty.packaged.min.js"></script>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css" media="all">
    </head>
    <body>
        <div id="container">
            <div id="header">
                <div id="spacer"></div>
                <a onclick ="refreshTable()" class="link-button">View Employees</a>   
                <a onclick ="refreshTableDept()" class="link-button" style = "right:0">View Departments</a>
            </div>
            <div id="content">
                <div id="title"></div>
                <div id = "pagination" class = "pagination"></div>
                <div id = "create" class = "table-button"><i class="fa fa-plus fa-2x"></i></div>
                <div id = "main-table"></div>
        </div>
        <div id = "formContainer" style = "display: none">
        <form id="employeeCreateForm" method="post" action="${pageContext.request.contextPath}/employee/create" style="visibility: none" title="Create employee">
            <table>
                <tr>
                    <td>Firstname:</td>
                    <td><input type="text" name="firstname" placeholder="John" required></td>
                </tr>
                <tr>
                    <td>Lastname:</td>
                    <td><input type="text" name="lastname" placeholder="Doe" required></td>
                </tr>
                <tr>
                    <td>Fathername:</td>
                    <td><input type="text" name="fathername"></td>
                </tr>
                <tr>
                    <td>Phone:</td>
                    <td><input type="text" name="phone" placeholder="123456789"></td>
                </tr>
                <tr>
                    <td>Department:</td>
                    <td><select name="department"></select></td>
                </tr>
                <tr>
                    <td>Salary:</td>
                    <td><input type="text" name="salary" placeholder="15000"></td>
                </tr>
            </table>      
        </form>
        <form id="employeeUpdateForm" method="post" action="${pageContext.request.contextPath}/employee/update" style="visibility: none" title="Update employee">
            <table>
                <tr>
                    <td>ID:</td>
                    <td>
                        <input type="text" name="visibleid" value="" required disabled>
                        <input type="hidden" name="id" value="">
                    </td>
                </tr>
                <tr>
                    <td>Firstname:</td>
                    <td><input type="text" name="firstname" placeholder="John" required></td>
                </tr>
                <tr>
                    <td>Lastname:</td>
                    <td><input type="text" name="lastname" placeholder="Doe" required></td>
                </tr>
                <tr>
                    <td>Fathername:</td>
                    <td><input type="text" name="fathername"></td>
                </tr>
                <tr>
                    <td>Phone:</td>
                    <td><input type="text" name="phone" placeholder="123456789"></td>
                </tr>
                <tr>
                    <td>Department:</td>
                    <td><select name="department"></select></td>
                </tr>
                <tr>
                    <td>Salary:</td>
                    <td><input type="text" name="salary" placeholder="100500"></td>
                </tr>
            </table>       
        </form>
        <form id="departmentCreateForm"  method="post" action="${pageContext.request.contextPath}/department/create" title="Create department">
            <table>
                <tr>
                    <td>Name:</td>
                    <td><input type="text" name="name" placeholder="Prinston Plainsboro" required></td>
                </tr>
                <tr>
                    <td>Head:
                    </td>
                    <td>
                        <input type="text" name="visiblehead">
                        <input type="hidden" name="head">
                    </td>
                </tr>
            </table>        
        </form>
        <form id="departmentUpdateForm"  method="post" action="${pageContext.request.contextPath}/department/update" title="Update department">
            <table>
                <tr>
                    <td>ID:</td>
                    <td>
                        <input type="text" name="visibleid" value="" required disabled>
                        <input type="hidden" name="id" value="">
                    </td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td><input type="text" name="name" placeholder="Prinston Plainsboro" required></td>
                </tr>
                <tr>
                    <td>Head:</td>
                    <td>
                        <input type="text" name="visiblehead">
                        <input type="hidden" name="head">
                    </td>
                </tr>
            </table>        
        </form>
            
        <div id="employeeDeleteForm" title="Delete employee?">
            <div id ="employeeDeleteFormMessage">
                <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This item will be permanently deleted and cannot be recovered. Are you sure?</p>
            </div>
        </div>
        
        <div id="departmentDeleteForm" title="Delete department?">
            <div id ="departmentDeleteFormMessage">
                <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This item will be permanently deleted and cannot be recovered. All employees from this department will be moved to Department #1. Are you sure?</p>
            </div>
        </div>
            
        </div>
        </div>
            
    <div id = "loadingOverlay">
        <div id = "loadingWrapper">
          <div id = "loadingContainer">
            <div id = "loadingLogo">
            </div>
            <div class="spinner">
              <div class="bounce1"></div>
              <div class="bounce2"></div>
              <div class="bounce3"></div>
            </div>
          </div>
        </div>
    </div>
    </body>
    <script>
        var parameters = {};
                
        $(document).ready(function(){
            refreshTable();
            fireNotification('success', 'loaded just fine');
        });
        
        function refreshTable(parameter) {
            if(parameter != null) $.extend(parameters, parameter); else parameters = {};
            $.post("${pageContext.request.contextPath}/employee/search", parameters, function(responseJson) {
                viewEmployees(responseJson);
            });
            $('#create').attr('onclick', 'createEmployee()');
        }
        
        function refreshTableDept(parameter) {
            if(parameter != null) $.extend(parameters, parameter); else parameters = {};
            $.post("${pageContext.request.contextPath}/department/search", parameters, function(responseJson) {
                viewDepartment(responseJson);
            });
            $('#create').attr('onclick', 'createDepartment()');
        }
        
        function viewEmployees(response) {
            var employees = response.data;
            var settings = response.settings[0];
            
            $.post("${pageContext.request.contextPath}/department/search", {allSearch: 1}, function(responseJson) {
                $("#main-table").html("");
                $("#pagination").html("");
                var departments = responseJson[0];
                var table = $('<table></table>').addClass('table-cust');
                table.append('<tr><td>ID</td><td>Firstname</td><td>Lastname</td><td>Fathername</td><td>Phone</td><td>Department</td><td>Salary</td><td></td><td></td></tr>');
                $.each(employees, function(i, employee) {
                    var row = $('<tr></tr>');
                    row.append('<td>' + employee.id + '</td>');
                    row.append('<td>' + employee.firstname + '</td>');
                    row.append('<td>' + employee.lastname + '</td>');
                    row.append('<td>' + employee.fathername + '</td>');
                    row.append('<td>' + employee.phone + '</td>');
                    var deptName;
                    $.each(responseJson.data, function(i, department) {
                        if(employee.department == department.id)
                            deptName = department.name;
                    });
                    row.append('<td class = "table-button" onclick="refreshTable({depart_id: ' + employee.department + '})">' + deptName + '</td>');
                    row.append('<td>' + employee.salary + '</td>');

                    row.append('<td class = "table-button edit" onclick="updateEmployee(' + employee.id + ')"><i class="fa fa-pencil-square-o fa-2x"></i></td>');
                    row.append('<td class = "table-button delete" onclick="deleteEmployee(' + employee.id + ')"><i class="fa fa-trash-o fa-2x"></i></td>');
                    table.append(row);
                });
                $('#main-table').append(table);
                pagination(settings);                                
            });
        }
        
        function pagination(settings) {
            if (settings.pages > 1) {
                var ul = $('<ul></ul>');
                if(settings.currentPage == '1') {
                    var li = $('<li></li>').addClass('active');
                    var span = $('<span>Prev</span>').addClass('current prev');
                    li.append(span);
                    ul.append(li);
                    li = $('<li></li>').addClass('active');
                    span = $('<span>1</span>').addClass('current');                        
                    li.append(span);
                    ul.append(li);
                } else {
                    var li = $('<li></li>');
                    var a = $('<a>Prev</a>').addClass('page-link prev');
                    a.attr('onclick', 'refreshTable({page: ' + (parseInt(settings.currentPage) - 1) + '})');
                    li.append(a);
                    ul.append(li);
                    li = $('<li></li>');
                    a = $('<a>1</a>').addClass('page-link');
                    a.attr('onclick', 'refreshTable({page: ' + 1 + '})');
                    li.append(a);
                    ul.append(li);
                }
                if (settings.currentPage - 1 > 3) {
                    var li = $('<li></li>').addClass('disabled');
                    var span = $('<span></span>').addClass('ellipse');
                    li.append(span);
                    ul.append(li);
                }
                for (var i = 2; i < parseInt(settings.pages); i++) {
                    if (parseInt(settings.currentPage) - i == 1 || i - parseInt(settings.currentPage) == 1) {
                        var li = $('<li></li>');
                        var a = $('<a>' + i + '</a>').addClass('page-link');
                        a.attr('onclick', 'refreshTable({page: ' + i + '})');
                        li.append(a);
                        ul.append(li);
                    }
                    if(parseInt(settings.currentPage) == i) {
                        var li = $('<li></li>');
                        var span = $('<span>' + i + '</span>').addClass('current');
                        li.append(span);
                        ul.append(li);
                    }
                }
                if(settings.currentPage == settings.pages) {
                    var li = $('<li></li>').addClass('active');
                    var span = $('<span>' + settings.pages + '</span>').addClass('current');                        
                    li.append(span);
                    ul.append(li);
                    li = $('<li></li>').addClass('active');                        
                    var span = $('<span>Next</span>').addClass('current next');
                    li.append(span);
                    ul.append(li);
                } else {
                    var li = $('<li></li>');
                    var a = $('<a>' + settings.pages + '</a>').addClass('page-link');
                    a.attr('onclick', 'refreshTable({page: ' + settings.pages + '})');
                    li.append(a);
                    ul.append(li);                        
                    li = $('<li></li>');
                    a = $('<a>Next</a>').addClass('page-link next');
                    a.attr('onclick', 'refreshTable({page: ' + (parseInt(settings.currentPage)+1) + '})');
                    li.append(a);
                    ul.append(li);                        
                }                    
                $('#pagination').append(ul);
            }        
        }
        
        function createEmployee() {
            var form = $("#employeeCreateForm");
            $.post("${pageContext.request.contextPath}/department/search", {allSearch: 1}, function(responseJson) {
                var new_options = '';
                $.each(responseJson.data, function(i, item) {
                    new_options += '<option value = "' + item.id + '">' + item.name + '</option>';
                    });
                $('#employeeCreateForm select').html(new_options);
            });
            var emplCreateDialog = form.dialog({
                height: 300,
                width: 350,
                modal: true,
                buttons: {
                    "Create employee": function() {
                        if(form[0].checkValidity()) {
                            $.post(form.attr("action"), form.serialize(), function(responseJson) {
                                if(responseJson.id != null) {
                                    fireNotification('success', 'Employee #' + responseJson.id + ' ' + responseJson.firstname + ' ' + responseJson.lastname + ' was successfully created');
                                    refreshTable("");
                                    emplCreateDialog.dialog( "close" );
                                } else {
                                    fireNotification('error', 'Something went wrong');
                                }
                            });
                        }
                    },
                    Cancel: function() {
                        emplCreateDialog.dialog( "close" );
                    }
                },
                close: function() {
                    form[0].reset();
                }
            });
        }
        
        function readEmployee() {
            
        }
        
        function updateEmployee(updateId) {
            $.post("${pageContext.request.contextPath}/employee/search", {exactId: updateId}, function(responseJson) {
                if(responseJson.data[0] != null) {
                    $("#employeeUpdateForm").attr('title', 'Update #' + updateId + ' ' + responseJson.data[0].firstname + ' ' + responseJson.data[0].lastname);
                    $("div[aria-describedby='employeeUpdateForm'] .ui-dialog-title").html('Update #' + updateId + ' ' + responseJson.data[0].firstname + ' ' + responseJson.data[0].lastname);
                    var form = $("#employeeUpdateForm");
                    $.post("${pageContext.request.contextPath}/department/search", {allSearch: 1}, function(responseJson) {
                        var new_options = '';
                        $.each(responseJson.data, function(i, item) {
                            new_options += '<option value = "' + item.id + '">' + item.name + '</option>';
                            });
                        $('#employeeUpdateForm select').html(new_options);
                    });

                    $.post("${pageContext.request.contextPath}/employee/search", {exactId: updateId}, function(responseJson) {
                        var employee = responseJson.data[0];
                        $("#employeeUpdateForm input[name='visibleid']").val(employee.id);
                        $("#employeeUpdateForm input[name='id']").val(employee.id);
                        $("#employeeUpdateForm input[name='firstname']").val(employee.firstname);
                        $("#employeeUpdateForm input[name='lastname']").val(employee.lastname);
                        $("#employeeUpdateForm input[name='fathername']").val(employee.fathername);
                        $("#employeeUpdateForm input[name='phone']").val(employee.phone);
                        var department = employee.department;
                        $("#employeeUpdateForm select option[value=" + department + "]").attr("selected", "selected");
                        $("#employeeUpdateForm input[name='salary']").val(employee.salary);
                    });

                    var emplUpdateDialog = form.dialog({
                        height: 300,
                        width: 350,
                        modal: true,
                        buttons: {
                            "Update employee": function() {
                                if(form[0].checkValidity()) {
                                    $.post(form.attr("action"), form.serialize(), function(responseJson) {
                                        if(responseJson.id != null) {
                                            fireNotification('success', 'Employee #' + responseJson.id + ' ' + responseJson.firstname + ' ' + responseJson.lastname + ' was successfully updated');
                                            refreshTable("");
                                            emplUpdateDialog.dialog("close");
                                        } else {
                                            fireNotification('error', 'Something went wrong');
                                        }
                                    });
                                }
                            },
                            Cancel: function() {
                                emplUpdateDialog.dialog( "close" );
                            }
                        },
                        close: function() {
                            form[0].reset();
                        }
                    });
                } else {
                    fireNotification("error", "Employee with ID #" + updateId + " doesn't exist");
                }  
            });
        }
        
        function deleteEmployee(deleteId) {
            $.post("${pageContext.request.contextPath}/employee/search", {exactId: deleteId}, function(responseJson) {
                if(responseJson.data[0] != null) {
                    $("#employeeDeleteForm").attr('title', 'Delete #' + deleteId + ' ' + responseJson.data[0].firstname + ' ' + responseJson.data[0].lastname + '?');
                    $("div[aria-describedby='employeeDeleteForm'] .ui-dialog-title").html('Delete #' + deleteId + ' ' + responseJson.data[0].firstname + ' ' + responseJson.data[0].lastname + '?');
                    var emplDeleteDialog = $("#employeeDeleteForm").dialog({
                        resizable: false,
                        height: 180,
                        width: 350,
                        modal: true,
                        buttons: {
                            "Delete this employee": function() {
                                $.post("${pageContext.request.contextPath}/employee/delete", {deleteId: deleteId}, function(responseJson) {
                                    if(responseJson.id != null) {
                                        fireNotification('success', 'Employee #' + responseJson.id + ' ' + responseJson.firstname + ' ' + responseJson.lastname + ' was successfully deleted');
                                        refreshTable("");
                                        emplDeleteDialog.dialog("close");
                                    } else {
                                        fireNotification('error', 'Something went wrong');
                                    }
                                });
                            },
                            Cancel: function() {
                                $(this).dialog("close");
                            }
                        }                    
                    });
                } else {
                    fireNotification("error", "Employee with ID #" + deleteId + " doesn't exist");
                }
            });
        }
        
        function viewDepartment(response) {
            var departments = response.data;
            var settings = response.settings[0];
            
            $("#main-table").html("");
            $("#pagination").html("");
            var table = $('<table></table>').addClass('table-cust');
            table.append('<tr><td>ID</td><td>Name</td><td>Head</td><td></td><td></td></tr>');
            $.each(departments, function(i, department) {
                var row = $('<tr></tr>');
                row.append('<td>' + department.id + '</td>');
                row.append('<td>' + department.name + '</td>');
                $.post("${pageContext.request.contextPath}/employee/search", {exactId: department.head_id}, function(responseJson) {
                    row.append('<td>' + responseJson.data[0].firstname + ' ' + responseJson.data[0].lastname + '</td>');
                    row.append('<td class = "table-button edit" onclick="updateDepartment(' + department.id + ')"><i class="fa fa-pencil-square-o fa-2x"></i></td>');
                    row.append('<td class = "table-button delete" onclick="deleteDepartment(' + department.id + ')"><i class="fa fa-trash-o fa-2x"></i></td>'); 
                });
                table.append(row);
            });
            $('#main-table').append(table);
            pagination(settings);
        }
        
        function createDepartment() {
            var form = $("#departmentCreateForm");            
            var emplNameAutocomplete = $("#departmentCreateForm input[name='visiblehead']");
            
            emplNameAutocomplete.autocomplete({
                appendTo: form,
                source: function(request, response) {
                    $.post("${pageContext.request.contextPath}/employee/search", {fullname: emplNameAutocomplete.val()}, function(responseJson) {
                        response(responseJson.data);
                    });
                },
                focus: function( event, ui ) {
                    emplNameAutocomplete.val(ui.item.firstname + " " + ui.item.lastname);
                    return false;
                },
                select: function( event, ui ) {
                    emplNameAutocomplete.val(ui.item.firstname + " " + ui.item.lastname);
                    $("#departmentCreateForm input[name='head']").val(ui.item.id);
                    return false;
                }
            })
            .autocomplete("instance")._renderItem = function( ul, item ) {
                return $( "<li>" ).append( "# " + item.id + " " + item.firstname + " " + item.lastname ).appendTo( ul );
            };
            
            var deptCreateDialog = form.dialog({
                height: 300,
                width: 350,
                modal: true,
                buttons: {
                    "Create department": function() {
                        if(form[0].checkValidity()) {
                            $.post(form.attr("action"), form.serialize(), function(responseJson) {
                                if(responseJson.id != null) {
                                    fireNotification('success', 'Department #' + responseJson.id + ' ' + responseJson.name + ' was successfully created');
                                    refreshTableDept("");
                                    deptCreateDialog.dialog("close");
                                } else {
                                    fireNotification('error', 'Something went wrong');
                                }
                            });
                        }
                    },
                    Cancel: function() {
                        deptCreateDialog.dialog("close");
                    }
                },
                close: function() {
                    form[0].reset();
                }
            });
        }
        
        function readDepartment() {
            
        }
        
        function updateDepartment(updateId) {
            $.post("${pageContext.request.contextPath}/department/search", {exactId: updateId}, function(responseJson) {
                if(responseJson.data != null) {
                    $("#departmentUpdateForm").attr('title', 'Update #' + updateId + ' ' + responseJson.data[0].name);
                    $("div[aria-describedby='departmentUpdateForm'] .ui-dialog-title").html('Update #' + updateId + ' ' + responseJson.data[0].name);
                    var form = $("#departmentUpdateForm");            
                    var emplNameAutocomplete = $("#departmentUpdateForm input[name='visiblehead']");            

                    $.post("${pageContext.request.contextPath}/department/search", {exactId: updateId}, function(responseJson) {
                        var department = responseJson.data[0];
                        $("#departmentUpdateForm input[name='visibleid']").val(department.id);
                        $("#departmentUpdateForm input[name='id']").val(department.id);
                        $("#departmentUpdateForm input[name='name']").val(department.name);
                        $("#departmenteUpdateForm input[name='head']").val(department.head_id);
                        var head;
                        $.post("${pageContext.request.contextPath}/employee/search", {exactId: department.head_id}, function(response) {
                            head = response.data[0];
                            var headname = head.firstname + " " + head.lastname;
                            $("#departmentUpdateForm [name='visiblehead']").val(headname);
                        });    
                    });

                    emplNameAutocomplete.autocomplete({
                        appendTo: form,
                        source: function(request, response) {
                            $.post("${pageContext.request.contextPath}/employee/search", {fullname: emplNameAutocomplete.val()}, function(responseJson) {
                                response(responseJson.data);
                            });
                        },
                        focus: function( event, ui ) {
                            emplNameAutocomplete.val(ui.item.firstname + " " + ui.item.lastname);
                            return false;
                        },
                        select: function( event, ui ) {
                            emplNameAutocomplete.val(ui.item.firstname + " " + ui.item.lastname);
                            $("#departmentUpdateForm input[name='head']").val(ui.item.id);
                            return false;
                        }
                    })
                    .autocomplete("instance")._renderItem = function( ul, item ) {
                        return $( "<li>" ).append( "# " + item.id + " " + item.firstname + " " + item.lastname ).appendTo( ul );
                    };

                    var deptUpdateDialog = form.dialog({
                        height: 300,
                        width: 350,
                        modal: true,
                        buttons: {
                            "Update department": function() {
                                if(form[0].checkValidity()) {
                                    $.post(form.attr("action"), form.serialize(), function(responseJson) {
                                        if(responseJson.id != null) {
                                            fireNotification('success', 'Department #' + responseJson.id + ' ' + responseJson.name + ' was successfully updated');
                                            refreshTableDept("");
                                            deptUpdateDialog.dialog("close");
                                        } else {
                                            fireNotification('error', 'Something went wrong');
                                        }
                                    });
                                }
                            },
                            Cancel: function() {
                                deptUpdateDialog.dialog("close");
                            }
                        },
                        close: function() {
                            form[0].reset();
                        }
                    });
                } else {
                    fireNotification("error", "Department with ID #" + updateId + " doesn't exist");
                } 
            });
        }
        
        function deleteDepartment(deleteId) {
            $.post("${pageContext.request.contextPath}/department/search", {exactId: deleteId}, function(responseJson) {
                if(responseJson.data != null) {
                    $("#departmentDeleteForm").attr('title', 'Delete #' + deleteId + ' ' + responseJson.data[0].name + '?');
                    $("div[aria-describedby='departmentDeleteForm'] .ui-dialog-title").html('Delete #' + deleteId + ' ' + responseJson.data[0].name + '?');
                    var deptDeleteDialog = $("#departmentDeleteForm").dialog({
                        resizable: false,
                        height: 180,
                        width: 350,
                        modal: true,
                        buttons: {
                            "Delete this department": function() {
                                $.post("${pageContext.request.contextPath}/department/delete", {deleteId: deleteId}, function(responseJson) {
                                    if(responseJson.id != null) {
                                        fireNotification('success', 'Department #' + responseJson.id + ' ' + responseJson.name + ' was successfully deleted');
                                        refreshTableDept("");
                                        deptDeleteDialog.dialog("close");
                                    } else {
                                        fireNotification('error', 'Something went wrong');
                                    }
                                });
                            },
                            Cancel: function() {
                                $(this).dialog("close");
                            }
                        }                    
                    });
                } else {
                    fireNotification("error", "Department with ID #" + deleteId + " doesn't exist");
                } 
            });
        }
        
        function fireNotification(type, text) {
            var n = noty({
                text: text,
                layout: 'bottomRight',
                type: type,
                timeout: 3000
            });
        }
        
    </script>
    
</html>
