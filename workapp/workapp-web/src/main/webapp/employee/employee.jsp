<%-- 
    Document   : worker
    Created on : 15.08.2015, 5:34:48
    Author     : daemon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="${pageContext.request.contextPath}/js/jquery-2.1.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/jquery-ui.js"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.noty.packaged.min.js"></script>
        <title>#${employee.id} ${employee.firstname} ${employee.lastname}</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui.css">        
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui.structure.css">        
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui.theme.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css" media="all">
    </head>
    <body>
        <div id="employee-name">
            #${employee.id} ${employee.lastname} ${employee.firstname} ${employee.fathername}
        </div>
        <table border="1">
            <tr>
                <td>ID</td>
                <td>${employee.id}</td>
            </tr>
            <tr>
                <td>First name</td>
                <td>${employee.firstname}</td>
            </tr>
            <tr>
                <td>Last name</td>
                <td>${employee.lastname}</td>
            </tr>
            <tr>
                <td>Father name</td>                
                <td>${employee.fathername}</td>
            </tr>
            <tr>
                <td>Phone</td>
                <td>${employee.phone}</td>
            </tr>
            <tr>
                <td>Department</td>
                <td>
                    <c:forEach items="${departments}" var="department">                            
                        <c:if test="${department.id == employee.department}">
                            <a href="${pageContext.request.contextPath}/department?id=${department.id}">${department.name}</a>
                        </c:if>
                    </c:forEach>
                </td>
            </tr>
            <tr>
                <td>Salary</td>                
                <td>${employee.salary}</td>
            </tr>
        </table>
        <p></p>
        <form action="${pageContext.request.contextPath}/employee/update" method="post">
            <input type="hidden" name="id" value="${employee.id}">
            <input type = "submit" value="Edit">
        </form>
        <p></p>
        <form action="${pageContext.request.contextPath}/employee/delete" method="post">
            <input type="hidden" name="id" value="${employee.id}">
            <input type = "submit" value="Delete">
        </form>

        <p></p>
        <a href="${pageContext.request.contextPath}/employee">All employees</a>
        <p></p>
        <a href="${pageContext.request.contextPath}/department">All departments</a>
            
            
    </body>
    <script>
        $(document).ready(function(){
            <c:if test="${created.id > 0}">
                var n = noty({
                    text: 'Employee was successfully created',
                    layout: 'bottomRight',
                    type: 'success'
                });
            </c:if>
        });
    </script>
</html>
