<%-- 
    Document   : workers
    Created on : 15.08.2015, 5:34:55
    Author     : daemon
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="${pageContext.request.contextPath}/js/jquery-2.1.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/jquery-ui.js"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.dynatable.js"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.noty.packaged.min.js"></script>
        <title>All employees</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui.css">        
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui.structure.css">        
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui.theme.css">       
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery.dynatable.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css" media="all">
    </head>
    <body>
        <form action="${pageContext.request.contextPath}/employee/create" method="post" >
            <p><input type="submit" value="Add new employee"></p>
        </form>   
            <a href="${pageContext.request.contextPath}/employee">All employees</a>
            <a href="${pageContext.request.contextPath}/department">All departments</a>
        <hr>
        <table id="table" border="1">
            <thead>
            <tr>
                <td>ID</td>
                <td>First name</td>
                <td>Last name</td>
                <td>Father name</td>
                <td>Phone</td>
                <td>Department</td>
                <td>Salary</td>
                <td></td>
                <td></td>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${employees}" var="employee">
                <tr>
                    <td style="text-align: center">
                        <a href="${pageContext.request.contextPath}/employee?id=${employee.id}">${employee.id}</a>
                    </td>
                    <td>${employee.firstname}</td>
                    <td>${employee.lastname}</td>
                    <td>${employee.fathername}</td>
                    <td>${employee.phone}</td>
                    <td>
                        <c:forEach items="${departments}" var="department">                            
                            <c:if test="${department.id == employee.department}">
                                <a href="${pageContext.request.contextPath}/department?id=${department.id}">${department.name}</a>
                            </c:if>
                        </c:forEach>
                    </td>
                    <td>${employee.salary}</td>
                    <td>
                        <form action="${pageContext.request.contextPath}/employee/update" method="post">
                            <input type="hidden" name="id" value="${employee.id}">
                            <input type = "submit" value="Edit">
                        </form>
                    </td>
                    <td>
                        <button value="${employee.id}" class="deleteBtn">Delete</button>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table> 
        
        <c:if test="${pages > 1}">
            <c:if test="${currentPage != 1}">
                <td><a href="${pageContext.request.contextPath}/employee?page=${currentPage - 1}">Previous</a></td>
            </c:if>

            <table border="1" cellpadding="5" cellspacing="5">
                <tr>
                    <c:forEach begin="1" end="${pages}" var="i">
                        <c:choose>
                            <c:when test="${currentPage eq i}">
                                <td>${i}</td>
                            </c:when>
                            <c:otherwise>
                                <td><a href="${pageContext.request.contextPath}/employee?page=${i}">${i}</a></td>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </tr>
            </table>

            <c:if test="${currentPage lt pages}">
                <td><a href="${pageContext.request.contextPath}/employee?page=${currentPage + 1}">Next</a></td>
            </c:if>
        </c:if>
                
        <form  id = "deletionForm" action="${pageContext.request.contextPath}/employee/delete" method="post">
            <input id="deletionFormInput" type="hidden" name="deleteId" value="">
        </form>
                
        <div id="dialog-confirm" title="Delete employee?">
            <div id ="dialog-message"></div>
        </div>
            
    </body>
    <script>
        $(document).ready(function() {
            $('#table').dynatable();
            <c:if test="${deleted.id > 0}">
                var n = noty({
                    text: 'Employee #${deleted.id} ${deleted.firstname} ${deleted.lastname} was successfully deleted',
                    layout: 'bottomRight',
                    type: 'success'
                });
            </c:if>
            <c:if test="${created.id > 0}">
                var n = noty({
                    text: 'Employee was successfully created',
                    layout: 'bottomRight',
                    type: 'success'
                });
            </c:if>            
        });
        
        $(".deleteBtn").click(function() {
            var recordId = this.value;
            $("#dialog-message").html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This item will be permanently deleted and cannot be recovered. Are you sure?</p>');
            $("#dialog-confirm").dialog({
                resizable: false,
                height: 180,
                width: 350,
                modal: true,
                buttons: {
                    "Delete this employee": function() {
                            $('#deletionFormInput').val(recordId);
                            $('#deletionForm').submit();
                    },
                    Cancel: function() {
                        $( this ).dialog( "close" );
                    }
                }                    
            });
        });
    </script>
</html>
