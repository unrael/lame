<%-- 
    Document   : create
    Created on : 17.08.2015, 23:12:05
    Author     : daemon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Create new employee</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css" media="all">
    </head>
    <body>
        <h2>Create new employee</h2>
        <form method="post" action="${pageContext.request.contextPath}/employee/create">
            <table>
                <tr>
                    <td>
                        Firstname:
                    </td>
                    <td>
                        <input type="text" name="firstname" placeholder="John" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        Lastname:
                    </td>
                    <td>
                        <input type="text" name="lastname" placeholder="Doe" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        Fathername:
                    </td>
                    <td>
                        <input type="text" name="fathername">
                    </td>
                </tr>
                <tr>
                    <td>
                        Phone:
                    </td>
                    <td>
                        <input type="text" name="phone" placeholder="123456789">
                    </td>
                </tr>
                <tr>
                    <td>
                        Department:
                    </td>
                    <td>
                        <select name="department">
                            <c:forEach var="item" items="${departments}">
                                <option value="${item.id}">${item.name}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        Salary:
                    </td>
                    <td>
                        <input type="text" name="salary" placeholder="15000">
                    </td>
                </tr>
            </table>
            
            <input type="submit" value="Create">            
        </form>
        <button onclick="javascript:history.back()">Cancel</button>
    </body>
</html>
