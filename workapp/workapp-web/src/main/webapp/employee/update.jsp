<%-- 
    Document   : update
    Created on : 18.08.2015, 6:12:42
    Author     : daemon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit employee #${employee.id}</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css" media="all">
    </head>
    <body>
        <h2>Edit #${employee.id} ${employee.lastname} ${employee.firstname} ${employee.fathername}</h2>
        <form method="post" action="${pageContext.request.contextPath}/employee/update">
            <table>
                <tr>
                    <td>
                        ID:
                    </td>
                    <td>
                        <input type="text" name="visibleid" value=${employee.id} required disabled>
                        <input type="hidden" name="id" value=${employee.id}>
                    </td>
                </tr>
                <tr>
                    <td>
                        Firstname:
                    </td>
                    <td>
                        <input type="text" name="firstname" value=${employee.firstname} required>
                    </td>
                </tr>
                <tr>
                    <td>
                        Lastname:
                    </td>
                    <td>
                        <input type="text" name="lastname" value=${employee.lastname} required>
                    </td>
                </tr>
                <tr>
                    <td>
                        Fathername:
                    </td>
                    <td>
                        <input type="text" name="fathername" value=${employee.fathername}>
                    </td>
                </tr>
                <tr>
                    <td>
                        Phone:
                    </td>
                    <td>
                        <input type="text" name="phone" value=${employee.phone}>
                    </td>
                </tr>
                <tr>
                    <td>
                        Department:
                    </td>
                    <td>
                        <select name="department">
                            <c:forEach var="item" items="${departments}">
                                <option value="${item.id}" <c:if test="${item.id == employee.department}">selected</c:if>>${item.name}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        Salary:
                    </td>
                    <td>
                        <input type="text" name="salary" value=${employee.salary} >
                    </td>
                </tr>
            </table>
            <input type="hidden" name="updated" value="1">
            <input type="submit" value="Save">            
        </form>
        <button onclick="javascript:history.back()">Cancel</button>        
    </body>    
</html>
