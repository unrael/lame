/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.web.generic;

import com.test.data.Department;
import com.test.data.Employee;
import com.test.data.SearchCriteria;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.test.data.accessobjects.EmployeeDAO;
import com.test.data.accessobjects.DepartmentDAO;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author daemon
 */
public class DepartmentServlet extends HttpServlet {
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DepartmentDAO departmentDAO = new DepartmentDAO();
        
        int page = 1;
        int recordsPerPage = 10;
        
        String departmentId = request.getParameter("id");
        if (departmentId != null && departmentId.length() != 0) {
            Department department = departmentDAO.read(Long.parseLong(departmentId));
            if (department == null) {
                request.setAttribute("message", "Department with ID #" + departmentId + " doesn't exist");
                request.getRequestDispatcher("error.jsp").forward(request, response);
            }
            EmployeeDAO employeeDAO = new EmployeeDAO();
            Employee head = employeeDAO.read(department.getHead_id());            
            
            if(request.getParameter("page") != null) {
                page = Integer.parseInt(request.getParameter("page"));
            }
            String limit = "LIMIT " + (page - 1) * recordsPerPage + "," + recordsPerPage;
            String where = "WHERE department = " + departmentId;
            int quantityOfEmployees = employeeDAO.entriesQuantity(where);
            int quantityOfPages = (int)Math.ceil(quantityOfEmployees * 1.0 / recordsPerPage);
            
            List<Employee> emplDept = employeeDAO.readAll(where,limit,"");
            request.setAttribute("department", department);            
            request.setAttribute("head", head);
            request.setAttribute("employees", emplDept);
            request.setAttribute("pages", quantityOfPages);
            request.setAttribute("currentPage", page);
            request.getRequestDispatcher("/department/department.jsp").forward(request, response);
        } else {
            
            List<Department> departments = departmentDAO.readAll("","","");
            List<Employee> heads = new ArrayList<Employee>();
            for (Department department : departments) {
                EmployeeDAO employeeDAO = new EmployeeDAO();
                Employee head = employeeDAO.read(department.getHead_id());
                heads.add(head);
            }
            request.setAttribute("departments", departments);
            request.setAttribute("heads", heads);
            if(request.getParameter("deleted") != null)
                request.setAttribute("deleted", request.getParameter("deleted"));
            request.getRequestDispatcher("/department/departments.jsp").forward(request, response);
        }         
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
