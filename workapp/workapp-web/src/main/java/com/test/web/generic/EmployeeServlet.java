/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.web.generic;

import com.test.data.Department;
import com.test.data.Employee;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.test.data.accessobjects.EmployeeDAO;
import com.test.data.accessobjects.DepartmentDAO;
import java.util.List;
/**
 *
 * @author daemon
 */
public class EmployeeServlet extends HttpServlet {
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        EmployeeDAO employeeDAO = new EmployeeDAO();
        DepartmentDAO departmentDAO = new DepartmentDAO();
        List<Department> departments = departmentDAO.readAll("","","");
        int page = 1;
        int recordsPerPage = 10;
               
        String employeeId = request.getParameter("id");
        if (employeeId != null && employeeId.length() != 0) {
            Employee employee = employeeDAO.read(Long.parseLong(employeeId));
            if (employee == null) {
                request.setAttribute("message", "Employee with ID #" + employeeId + " doesn't exist");
                request.getRequestDispatcher("error.jsp").forward(request, response);
            }            
            request.setAttribute("employee", employee);
            request.setAttribute("departments", departments);
            request.getRequestDispatcher("/employee/employee.jsp").forward(request, response);
        } else {
            if(request.getParameter("page") != null) {
                page = Integer.parseInt(request.getParameter("page"));
            }
            int quantityOfEmployees = employeeDAO.entriesQuantity("");
            int quantityOfPages = (int)Math.ceil(quantityOfEmployees * 1.0 / recordsPerPage);
            if (quantityOfPages < page) {
                request.setAttribute("message", "Page #" + page + " doesn't exist");
                request.getRequestDispatcher("error.jsp").forward(request, response);
            }
            
            String limit = "LIMIT " + (page-1)*recordsPerPage + "," + recordsPerPage;
            List<Employee> employees = employeeDAO.readAll("",limit,"");
            request.setAttribute("employees", employees);            
            request.setAttribute("departments", departments);       
            request.setAttribute("pages", quantityOfPages);
            request.setAttribute("currentPage", page);
            request.setAttribute("created", request.getParameter("created")); 
            request.getRequestDispatcher("/employee/employees.jsp").forward(request, response);
        }         
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
