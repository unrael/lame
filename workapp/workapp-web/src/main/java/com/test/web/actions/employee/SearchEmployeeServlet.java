/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.web.actions.employee;

import com.google.gson.Gson;
import com.test.data.Employee;
import com.test.data.SearchCriteria;
import static com.test.data.SearchCriteria.FULLNAME;
import com.test.data.accessobjects.EmployeeDAO;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author daemon
 */
public class SearchEmployeeServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        EmployeeDAO employeeDAO = new EmployeeDAO();
        
        response.setContentType("text/html; charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        
        int page = 1;
        int recordsPerPage = 10;
        String where = "";       
        String limit = "LIMIT " + ((page - 1) * recordsPerPage) + ", " + recordsPerPage;
        String order = "";        
        
        Enumeration<String> parameters = request.getParameterNames();       
        while (parameters.hasMoreElements()) {
            String parameter = parameters.nextElement();
            String[] parameterValues = request.getParameterValues(parameter);
            switch(parameter) {
                case "exactId":
                    if (!parameter.isEmpty()) {
                        if (where != null && where.length() != 0) where += ',';
                        where += "id LIKE '" + parameterValues[0].toString() + "'";
                    }
                    break;
                case "fullname":
                    if (!parameter.isEmpty()) {
                        if (where != null && where.length() != 0) where += ',';
                        where += "CONCAT(firstname,  ' ', lastname) LIKE '%" + parameterValues[0].toString() + "%'";
                    }
                    break;
                case "phone":
                    if (!parameter.isEmpty()) {
                        if (where != null && where.length() != 0) where += ',';
                        where += "phone LIKE '%" + parameterValues[0].toString() + "%'";
                    }
                    break;
                case "depart_id":
                    if (!parameter.isEmpty()) {
                        if (where != null && where.length() != 0) where += ',';
                        where += "department LIKE '%" + parameterValues[0].toString() + "%'";
                    }
                    break;
                case "page":
                    if (!parameter.isEmpty()) {
                        page = Integer.parseInt(parameterValues[0].toString());
                        limit = "LIMIT " + (page - 1) * recordsPerPage + ", " + recordsPerPage;
                    }                    
                    break;
                default:
                    break;
            }
        }
        
        if(where != null && where.length() != 0) {
            where = "WHERE " + where;
        }
        
        int records = employeeDAO.entriesQuantity(where);
        int pages = (int)Math.ceil(records * 1.0 / recordsPerPage);
        
        List<Employee> employees = employeeDAO.readAll(where, limit, order);
        
        boolean ajax = "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
        if (ajax) {
            String json1 = new Gson().toJson(employees);
            String json = "{\"data\":" + json1 + ",\"settings\":[{\"currentPage\": \"" + page + "\", \"pages\": \"" + pages + "\", \"records\": \"" + records + "\"}]}";
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json);
        }        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
