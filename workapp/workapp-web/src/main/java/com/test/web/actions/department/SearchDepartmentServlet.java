/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.web.actions.department;

import com.google.gson.Gson;
import com.test.data.Department;
import com.test.data.accessobjects.DepartmentDAO;
import java.io.IOException;
import java.util.Enumeration;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author daemon
 */
public class SearchDepartmentServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DepartmentDAO departmentDAO = new DepartmentDAO();
        
        response.setContentType("text/html; charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        
        int page = 1;
        int recordsPerPage = 10;
        String where = "";       
        String limit = "LIMIT " + ((page - 1) * recordsPerPage) + ", " + recordsPerPage;
        String order = "";        
        
        Enumeration<String> parameters = request.getParameterNames();       
        while (parameters.hasMoreElements()) {
            String parameter = parameters.nextElement();
            String[] parameterValues = request.getParameterValues(parameter);
            switch(parameter) {
                case "exactId":
                    if (!parameter.isEmpty()) {
                        if (where != null && where.length() != 0) where += ',';
                        where += "id LIKE '" + parameterValues[0].toString() + "'";
                    }
                    break;
                case "name":
                    if (!parameter.isEmpty()) {
                        if (where != null && where.length() != 0) where += ',';
                        where += "CONCAT(name) LIKE '%" + parameterValues[0].toString() + "%'";
                    }
                    break;
                case "allDept":
                    if (!parameter.isEmpty()) {
                        if (where != null && where.length() != 0) where += ',';
                        where = "";
                        limit = "";
                    }
                    break;
                default:
                    break;
            }
        }
        
        if(where != null && where.length() != 0) {
            where = "WHERE " + where;
        }
        
        int records = departmentDAO.entriesQuantity(where);
        int pages = (int)Math.ceil(records * 1.0 / recordsPerPage);
        
        
        List<Department> departments = departmentDAO.readAll(where, limit, order);
        
        boolean ajax = "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
        if (ajax) {
            String json1 = new Gson().toJson(departments);
            String json = "{\"data\":" + json1 + ",\"settings\":[{\"currentPage\": \"" + page + "\", \"pages\": \"" + pages + "\", \"records\": \"" + records + "\"}]}";
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json);
        } else {
            
        } 
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
